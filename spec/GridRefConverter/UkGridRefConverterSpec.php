<?php

namespace spec\GridRefConverter;

use GridRefConverter\UkGridRefConverter;
use GridRefConverter\GridRefConverter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UkGridRefConverterSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UkGridRefConverter::class);
    }

    function it_should_implement_grid_ref_convertor_interface()
    {
        $this->shouldImplement(GridRefConverter::class);
    }

    function let()
    {
        $this->beConstructedThrough('withGridRef', ['NN166712']);
    }

    function it_should_have_an_alpha()
    {
        $this->getAlpha()->shouldReturn('NN');
    }

    function it_should_have_a_beta()
    {
        $this->getBeta()->shouldReturn(166);
    }

    function it_should_have_a_ceta()
    {
        $this->getCeta()->shouldReturn(712);
    }

    function it_should_have_an_easting()
    {
        $this->getEasting()->shouldReturn(2166000);
    }

    function it_should_have_a_northing()
    {
        $this->getNorthing()->shouldReturn(7712000);
    }
}
