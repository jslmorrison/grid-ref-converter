<?php

namespace GridRefConverter;

interface GridRefConverter
{
    public static function withGridRef($gridRef);
    public function getEasting();
    public function getNorthing();
}