<?php

namespace GridRefConverter;

final class UKGridReferences
{
    // just a sample, covers the Munro areas
    const GRIDREFS = [
        '17' => 'NM',
        '18' => 'NG',
        '27' => 'NN',
        '28' => 'NH',
        '29' => 'NC',
        '37' => 'NO',
        '38' => 'NJ'
    ];
}