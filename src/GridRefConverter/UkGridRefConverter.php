<?php

namespace GridRefConverter;

use GridRefConverter\GridRefConverter;
use GridRefConverter\UKGridReferences;

class UkGridRefConverter implements GridRefConverter
{
    protected $gridRef;
    protected $alpha;
    protected $beta;
    protected $ceta;

    private function __construct()
    {
    }

    public static function withGridRef($gridRef)
    {
        $ukGridRefConverter = new UkGridRefConverter();
        $ukGridRefConverter->gridRef = $gridRef;

        return $ukGridRefConverter;
    }

    public function getAlpha()
    {
        return substr($this->gridRef, 0, 2);
    }
 
    public function getBeta()
    {
        $beta = substr($this->gridRef, 2);
        return intval(substr($beta, 0, strlen($beta) / 2));
    }

    public function getCeta()
    {
        $ceta = substr($this->gridRef, 2);
        return intval(substr($ceta, strlen($ceta) / 2));
    }

    public function getEasting()
    {
        $prefix = (string)array_search($this->getAlpha(), UKGridReferences::GRIDREFS);
        return intval(substr($prefix, 0, 1).$this->getBeta() * 1000);
    }

    public function getNorthing()
    {
        $prefix = (string)array_search($this->getAlpha(), UKGridReferences::GRIDREFS);
        return intval(substr($prefix, 1, 1).$this->getCeta() * 1000);
    }
}
